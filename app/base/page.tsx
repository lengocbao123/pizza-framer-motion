"use client";
import { cx } from "class-variance-authority";
import Link from "next/link";
import { Fragment, useMemo, useState } from "react";
import ChevronRightIcon from "../components/icons/chevron-right";
import { motion } from "framer-motion";

export default function Base() {
  const [active, setActive] = useState("");
  const handleClick = (value: string) => {
    setActive(value);
  };
  const options = useMemo(
    () => [
      { id: "classic", name: "Classic" },
      { id: "thin_crispy", name: "Thin & Crispy" },
      { id: "thick_crust", name: "Thick Crust" },
    ],
    []
  );
  return (
    <motion.div
      className="flex flex-col gap-10"
      initial={{
        x: "100vw",
      }}
      animate={{
        x: "0",
      }}
      transition={{
        duration: 0.5,
      }}
    >
      <h2 className="text-white text-xl border-b border-b-gray-400 py-4">
        Step 1: Choose Your Base
      </h2>
      <ul className="flex flex-col text-white gap-4 w-full">
        {options.map((option) => (
          <li
            key={option.id}
            className={cx(
              "hover:font-bold cursor-pointer flex items-center gap-1",
              active === option.id && "font-bold"
            )}
            onClick={() => handleClick(option.id)}
          >
            {active === option.id && <ChevronRightIcon />}{" "}
            <span>{option.name}</span>
          </li>
        ))}
      </ul>

      {active && (
        <Link href="/toppings">
          <motion.button
            initial={{
              x: "-100vw",
            }}
            animate={{
              x: "0",
            }}
            transition={{
              duration: 0.5,
            }}
            className="border-2 rounded-full border-white px-5 py-2 text-white text-lg"
          >
            Next
          </motion.button>
        </Link>
      )}
    </motion.div>
  );
}
