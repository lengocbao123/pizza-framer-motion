"use client";
import Link from "next/link";
import { Fragment } from "react";
import { motion } from "framer-motion";

export default function Home() {
  return (
    <motion.div
      drag="x"
      whileDrag={{ x: 100 }}
      className="flex flex-col items-center gap-10 "
    >
      <motion.h2
        className="text-white text-center whitespace-nowrap"
        initial={{
          fontSize: "0px",
        }}
        animate={{
          fontSize: "50px",
        }}
        transition={{
          duration: 0.5,
        }}
      >
        Welcome to Pizza Joint
      </motion.h2>
      <Link href="/base">
        <motion.button
          className="border-2 rounded-full border-white px-5 py-2 text-white text-lg"
          initial={{
            display: "none",
          }}
          animate={{
            display: "block",
          }}
          transition={{
            duration: 0.5,
          }}
        >
          Create Your Pizza
        </motion.button>
      </Link>
    </motion.div>
  );
}
