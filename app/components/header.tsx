"use client";
import React from "react";

const Header = () => {
  return (
    <header className="flex p-10 items-center gap-2 text-white">
      <div className="">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className="w-20 h-20"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M10.5 6a7.5 7.5 0 107.5 7.5h-7.5V6z"
          />
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M13.5 10.5H21A7.5 7.5 0 0013.5 3v7.5z"
          />
        </svg>
      </div>
      <div className="flex-grow border-b border-b-gray-400 py-4">
        <h1 className="text-2xl">Pizza Joint</h1>
      </div>
    </header>
  );
};

export default Header;
