"use client";
import { cx } from "class-variance-authority";
import { Fragment, useMemo, useState } from "react";
import ChevronRightIcon from "../components/icons/chevron-right";
import Link from "next/link";
import { motion } from "framer-motion";

export default function Toppings() {
  const [active, setActive] = useState("");
  const handleClick = (value: string) => {
    setActive(value);
  };
  const options = useMemo(
    () => [
      { id: "mushrooms", name: "mushrooms" },
      { id: "peppers", name: "peppers" },
      { id: "onions", name: "onions" },
      { id: "olives", name: "olives" },
      { id: "extra_cheese", name: "extra cheese" },
      { id: "tomatoes", name: "tomatoes" },
    ],
    []
  );
  return (
    <Fragment>
      <h2 className="text-white text-xl border-b border-b-gray-400 py-4">
        Step 2: Choose Toppings
      </h2>
      <ul className="flex flex-col text-white gap-4 w-full">
        {options.map((option) => (
          <motion.li
            key={option.id}
            whileHover={{
              scale: 1.3,
              color: "#CA8A04",
            }}
            className={cx(
              "hover:font-bold cursor-pointer flex items-center gap-1",
              active === option.id && "font-bold"
            )}
            onClick={() => handleClick(option.id)}
          >
            {active === option.id && <ChevronRightIcon />}
            <span>{option.name}</span>
          </motion.li>
        ))}
      </ul>
      {active && (
        <Link href="/order">
          <motion.button
            initial={{
              x: "-100vw",
            }}
            animate={{
              x: "0",
            }}
            className="border-2 rounded-full border-white px-4 py-2 text-white"
          >
            Next
          </motion.button>
        </Link>
      )}
    </Fragment>
  );
}
