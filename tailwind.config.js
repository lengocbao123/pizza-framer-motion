/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(circle, #64007b 0%,#3e1456 100%)",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        primary: {
          DEFAULT: "#64007b",
          50: "#fdf4ff",
          100: "#f9e6ff",
          200: "#f4d2ff",
          300: "#edaeff",
          400: "#e27bff",
          500: "#d649ff",
          600: "#ca25f8",
          700: "#b115db",
          800: "#9317b2",
          900: "#78148f",
        },
      },
    },
  },
  plugins: [],
};
